#!/usr/bin/python

import os
import glob


LINKS = {
    "redis_": ["redis_connected_clients", "redis_per_sec", "redis_used_memory"],
}

# Don't use this, use the variable in the playbook instead.
BLACKLIST = []


def main():
    module = AnsibleModule(
        argument_spec = dict(
            services = dict(default=[]),
            blacklist = dict(default=[]),
            dirs = dict(default=['./']),
        )
    )
    services = module.params['services']
    blacklist = list(set(module.params['blacklist'] + BLACKLIST))
    dirs = module.params['dirs']

    # paths to the plugin containing dirs
    dirpaths = []
    for service in services:
        dirpaths += map(lambda dir: os.path.join(dir, service), dirs)
    dirpaths = filter(lambda path: os.path.isdir(path), dirpaths)

    # paths to the plugins themselves
    plugin_paths = []
    for path in dirpaths:
        plugin_paths += glob.glob(os.path.join(path, '*'))
    
    # reshape
    data = []
    for path in plugin_paths:
        name = os.path.basename(path)
        data.append({
            "name": name,
            "src": path,
            "links": LINKS.get(name, [name]),
        })            

    # postprocessing: apply blacklist
    for obj in data:
        obj["links"] = list(set(obj["links"]) - set(blacklist))

    module.exit_json(out=data)


from ansible.module_utils.basic import *
main()

