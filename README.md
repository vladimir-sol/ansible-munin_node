A playbook delivering EC2-tag-driven monitoring plugin set to instances,
written in 2015.


---


TODO: Rewrite the plugin enabling mechanism. (Restructure to
{ec2_tag_munin_plugins.piece: [enabled_plugin_name]}; keep the current style of
lookup and delivery with a defined end result (enabled_plugins names) that would
also be able to locate their link target in a smart way)


Ansible role dependencies:

* geerlingguy.munin-node
* geerlingguy.apache [initial munin-server install only]


Required submodules:

* ansible-role-munin for munin-server playbook
* contrib for munin-node playbook plugin functionality (https://github.com/munin-monitoring/contrib)


To add a server to monitoring:

1. Set an ec2 tag – munin:true
2. (Optional) ec2 tag - munin_plugins:plugin1,plugin2,plugin3 (dir names)
3. Run munin-node.yml playbook to install munin-node, munin-server.yml playbook to update server configuration


To author custom plugins:

0. Contrib repo (added as a submodule at ./contrib) has a wide selection of community plugins, check that first.
1. Create a subfolder in ./plugins (you will later use it's name in munin_plugins ec2 tag)
2. Create your script there. It's safe to assume that bash, python, perl are available on all machines
3. Follow munin guidlines for writing plugins (http://munin-monitoring.org/wiki/HowToWritePlugins)
4. Commit, push, run the munin jenkins job.


#### Tag

Requires:

boto (python module)


NOTE: umbrella is not automated!  
NOTE2: per-node plugin configuration is manual
